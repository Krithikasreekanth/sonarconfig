package com.springboot;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
@Transactional
public class PersonDAO {
@Autowired
private SessionFactory sessionFactory;
	public void delete(Person person) {
		sessionFactory.getCurrentSession().delete(person);
		}

	public void savePerson(Person person) {
		
	sessionFactory.getCurrentSession().save(person);	
	}

	public List<Person> getAllPersons() {
		return sessionFactory.getCurrentSession().createQuery("from Person").list();
		
	}

}
